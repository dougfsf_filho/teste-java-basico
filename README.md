Nesta prova será necessário implementar os métodos das classes conforme descrições dos Javadocs correpondentes.

Você poderá criar novos atributos e métodos, mas é proibido mover as classes de pacote. Também é proibido mudar a assinatura dos métodos e construtores já existentes, exceto o construtor da classe CarrinhoCompras.

A prova deve ser resolvida utilizando obrigatoriamente Java 8. Não será necessário implementar nenhum tipo de interface gráfica ou mecanismo de persistência.

Todos os requisitos funcionais tem que ser atendidos 100% para que sua solução seja aprovada. Sua prova precisa compilar via Maven, impreterivelmente - não basta rodar apenas via sua IDE.

Todos os prerrequisitos de construção de objetos se encontram nas anotações de cada classe, é imprescindível o uso de testes unitários que comprovem a eficácia do código escrito.

Não é permitido o uso de Framworks de apoio, apenas Java 8.